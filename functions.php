﻿<?php 

define('THEMEROOT', get_stylesheet_directory_uri());
define('IMG', THEMEROOT . '/img');


add_filter('widget_text', 'do_shortcode');
add_filter( 'the_excerpt', 'do_shortcode');
remove_filter ('the_content', 'wpautop');
//include_once('includes/carousel.php' );
//include_once('includes/shortcodes.php');

add_theme_support( 'post-thumbnails' );

add_image_size( 'ikonka', 60, 65, true );

add_image_size( 'logo', 150, 170, true );

include_once('includes/style.php');
include_once('includes/footer-functions.php');
include_once('includes/settings.php');
include_once('includes/register_post.php');



function register_my_menus() {
  register_nav_menus(
    array(
      'header-menu' => __( 'Menu Top' ),
    )
  );
}
add_action( 'init', 'register_my_menus' );


include_once('includes/shortcodes.php');
include_once('includes/sidebar.php');

function theme_grab_galleries_photos($galleryContent) {
	
	preg_match_all('/\[gallery(.*?)ids="(.*?)"(.*?)\]/', $galleryContent, $galleryContentMatches);
	$galleryContentIds = array();

	foreach($galleryContentMatches[2] as $row) {
		foreach(explode(',', $row) as $id) {
			if(($id = trim($id)) && is_numeric($id))
				$galleryContentIds[] = $id;
		}
	}
	
	if(0 == count($galleryContentIds))
		return array();
	
	$args = array(
		'post_type' => 'attachment',
		'numberposts' => -1,
		'post_status' => null,
		'post__in' => $galleryContentIds,
	);

	return get_posts( $args );
}
