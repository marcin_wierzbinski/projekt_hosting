<?php 
/* Template Name: Home2 */
?>
<?php 
get_header(); 
$galleryContent = get_post_meta($post->ID, 'galeria', true);
$attachments = theme_grab_galleries_photos($galleryContent); 
?>

	<div class="slider">
        <div class="container">
            <div class="row">
				<?php 
				if(have_posts()) : the_post(); 
				the_content(); 
				endif;
				?> 
            </div>
        </div>
        <!-- /.container -->
	</div>
    <!-- /.slider -->
    
    <section class="bar-header">
        <div class="container">
            <div class="row">
                <div class="col-md-6 col-sm-12">
                    <div class="row value">
                        <div class="col-sm-9 col-xs-9 ">
                            <div class="row">
                                <div class="col-md-9 col-xs-9 pad-right">
                                    <input type="text" class="form-control" placeholder="Username">
                                </div>
                                <div class="col-md-3 col-xs-3 pad-left">
                                    <select class="form-control special"> <option>.com</option><option>.pl</option></select>
                                </div>
                            </div>
                            <p><?php echo get_option('section5'); ?></p>
                        </div>
                        <div class="col-sm-3 col-xs-3 ">
                            <button>Wyszukaj</button>
                        </div>
                    </div>
                </div>
                <div class="col-md-6 col-sm-12">
                    <div class="row">
                      <?php dynamic_sidebar( 'sidebar-11' ); ?>
                    </div>
                </div>
            </div>
        </div>
    </section>
    <section class="offer">
        <div class="container">
            <div class="divider-30"></div>
            <div class="row">
					<?php 
					$query = new WP_Query( array( 'post_type' => 'offer', 'nopaging' => 'true' ) );
					if($query->have_posts()) : while($query->have_posts()) : $query->the_post();
					?>
					<div class="col-md-3 col-xs-6">
						<div class="row">
							<div class="col-sm-3 col-md-3 col-xs-3">
								<?php 
								if ( has_post_thumbnail() ) {
								the_post_thumbnail('ikonka', array('class' => 'img-responsive'));
								}
								?>
							</div>
							<div class="col-sm-9 col-md-9 col-xs-9 text-left">
								<h2><?php the_title(); ?></h2>
							</div>                   
						</div>
						<div class="divider-30"> </div>
						  <?php the_excerpt(); ?> 
						  <div class="divider-40"></div>
						<a href="<?php echo  get_permalink($post->ID); ?>">Details <span class="glyphicon glyphicon-circle-arrow-right"></span></a>
					</div>
					<?php
					endwhile;
					endif;
					?>
            </div>
            <div class="divider-60"></div>
        </div>
    </section>
    <section class="content-section-a">

        <div class="container">
            
            <div class="text-center">
                <h2><?php echo get_option('section1'); ?></h2></div>
            <div class="divider-25"></div>
            <div class="row">
			<?php dynamic_sidebar( 'sidebar-1' ); ?>
            </div>

        </div>
        <!-- /.container -->

    </section>
    <!-- /.content-section-a -->

    <div class="content-section-b">
			<div class="container">
			  <div class="text-center"><h2><?php echo get_option('section2'); ?></h2></div>
			<div class="row">
					<?php 
						if ( $attachments ) {
						foreach ( $attachments as $attachment ) {
						$attachmenImage = wp_get_attachment_image_src($attachment->ID, 'logo');
						list($attachmentImageUrl) = $attachmenImage;	
						echo '<div style="margin-top:20px;" class="col-md-2 col-xs-3 text-center">';
						echo "<img src=".$attachmentImageUrl." />";
						echo '</div>';
						}
						}									
						?>
			</div>  
			</div>
    </div>
    <!-- /.content-section-a -->
    <section class="news">
       <div class="container"> 
	   <div class="row">
            <div class="col-lg-12">
                <div class="row"><h2><?php echo get_option('section3'); ?></h2>
				<?php
				$query = new WP_Query( array( 'post_type' => 'reviews', 'nopaging' => 'true' ) );
				$number = 0; 
				 if($query->have_posts()):
				?>
			<div id="myCarousel3" class="carousel slide">
			  <ol class="carousel-indicators">
				<?php  while($query->have_posts()): $query->the_post(); ?>
				<li data-target="#myCarousel3" data-slide-to="<?php echo $number++; ?>"></li>
				<?php  endwhile; ?>
			  </ol>

			  <!-- Carousel items -->
			  <div class="carousel-inner">
				<?php while($query->have_posts()): $query->the_post(); ?>
				 <?php ?>
				<div class="item">
                    <div class="row">
                        <div class="testimonials text-center">
                                <img src="<?php print IMG ?>/cytat.png" alt="icon" />
									<p><?php the_content(); ?></p>
                                <img src="<?php print IMG ?>/cytat2.png" alt="icon" />
                        </div>
						<div class="author_reviews"><?php the_title(); ?></div>
                    </div>
                </div> 
				<?php endwhile; ?>
			  </div>

			  <!-- Carousel nav - these chevrons require the Font Awesome library to be loaded. It often is with Bootstrap themes -->
			  
			  <a class="carousel-control left" href="#myCarousel3" data-slide="prev"><i class="icon-chevron-left icon-2x"></i></a>
			  <a class="carousel-control right" href="#myCarousel3" data-slide="next"><i class="icon-chevron-right icon-2x"></i></a>
			</div>
			<?php endif; wp_reset_query();?>
               <div class="divider-40"> </div>     
            </div> 
			</div>
       </div>
    </section>  
    <!-- /.banner -->

    <?php get_footer(); ?>
