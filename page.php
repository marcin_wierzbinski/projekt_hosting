<?php 
/* Template Name: Home2 */
?>
<?php 
get_header(); 
?>

	<div class="slider">
        <div class="container">
				<?php 
				if(have_posts()) : the_post(); 
				the_content(); 
				endif;
				?> 
        </div>
        <!-- /.container -->
	</div>

    <?php get_footer(); ?>
