<!DOCTYPE html>
<html <?php language_attributes(); ?>>
<head>
    <meta charset="<?php bloginfo('charset'); ?>">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta name="description" content="">
    <meta name="author" content="">

    <title><?php bloginfo('name'); ?> <?php wp_title('|', true, 'left'); ?></title>
	<link href="<?php bloginfo('stylesheet_url'); ?>" rel="stylesheet"/>
	<?php wp_head(); ?>
	<style>
	.img_style{
	cursor:pointer;
	}
	
	
	</style>
    <link href='http://fonts.googleapis.com/css?family=Lato:100,300,400,700,900,100italic,300italic,400italic,700italic,900italic' rel='stylesheet' type='text/css'>
    <link href='http://fonts.googleapis.com/css?family=Raleway:400,100,200,300,600,500,700,800,900' rel='stylesheet' type='text/css'>

</head>

<body>
    <div class="top-bar">
		<div class="container">
			<div class="row">
				<div class="col-md-4 col-xs-8">
				<?php dynamic_sidebar( 'sidebar-8' ); ?>
				</div>
				<div class="col-md-5 col-xs-1">
				<?php dynamic_sidebar( 'sidebar-9' ); ?>
				</div>
				<div class="col-md-3 col-xs-3">
				<?php dynamic_sidebar( 'sidebar-10' ); ?>
				</div>
			</div>
		</div>
    </div>
    <nav class="navbar navbar-default navbar-fixed-top" role="navigation">
        <div class="container">
            <div class="navbar-header">
                <button type="button" class="navbar-toggle" data-toggle="collapse" data-target=".navbar-ex1-collapse">
                    <span class="sr-only">Toggle navigation</span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                    <span class="icon-bar"></span>
                </button>
               <a href="<?php echo home_url(); ?>" id="logo"><img src="<?php print IMG ?>/logo.png" alt="logo" /></a>
            </div>

            <!-- Collect the nav links, forms, and other content for toggling -->
            <div class="collapse navbar-collapse navbar-right navbar-ex1-collapse">
                    <?php
					$defaults = array(
						'theme_location'  => 'header-menu',
						'menu_class'      => 'top-bar',
						'echo'            => true,
						'fallback_cb'     => 'wp_page_menu',
						'items_wrap'      => '<ul class="nav navbar-nav">%3$s</ul>',
						'depth'           => 0,
					);
					wp_nav_menu( $defaults );
					?>
            </div>
            <!-- /.navbar-collapse -->
        </div>
        <!-- /.container -->
    </nav>
