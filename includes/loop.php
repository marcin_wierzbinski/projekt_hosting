<div id="content">
    <?php
$new_query = new WP_Query();
$new_query->query('post_type=post&showposts=1'.'&paged='.$paged);
?>
<?php if ( have_posts() ) : ?>
<?php while ($new_query->have_posts()) : $new_query->the_post(); ?>
<?php the_title(); ?>

<?php endwhile;
endif;
 ?>
    <div id="pagination">
    <?php next_posts_link('&laquo; Older Entries', $new_query->max_num_pages) ?>
    <?php previous_posts_link('Newer Entries &raquo;') 
	?>
    </div>
</div><!-- #content -->

<script>
jQuery(function($) {
    $('#content').on('click', '#pagination a', function(e){
        e.preventDefault();
        var link = $(this).attr('href');
        $('#content').fadeOut(500, function(){
            $(this).load(link + ' #content', function() {
                $(this).fadeIn(500);
            });
        });
    });
});
</script>