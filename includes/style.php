<?php 
	function hosting_scripts() {
	/* Global Style*/
	wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css' );
	wp_enqueue_style( 'fontAwesome', get_template_directory_uri() . '/font-awesome/css/font-awesome.min.css', array('bootstrap') );
	wp_enqueue_style( 'landingPage', get_template_directory_uri() . '/css/landing-page.css', array('fontAwesome') );
	wp_enqueue_style( 'carousel', get_template_directory_uri() . '/css/carousel.css', array('landingPage') );
	
	wp_enqueue_script('jquery',get_stylesheet_directory_uri() . '/js/jquery-1.10.2.js"',array( 'jquery' ), '1.0.0', true);
	wp_enqueue_script('bootstrap',get_stylesheet_directory_uri() . '/js/bootstrap.js',array( 'jquery' ), '1.0.0', true);
	/* End Global Style */
	
	
}
add_action( 'wp_enqueue_scripts', 'hosting_scripts' );