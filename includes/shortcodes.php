<?php
if (!function_exists('button')) {
function button($atts, $content = null) {
extract(shortcode_atts(array('href' => ''), $atts));
	return '<button>' . do_shortcode($content) . '</button>';
}
}
add_shortcode('button', 'button');

if (!function_exists('facebook')) {
function facebook($atts, $content = null) {
extract(shortcode_atts(array('href' => ''), $atts));
	return '<div class="min_ikon"><a href="'.$href .'"><i class="fa fa-facebook"></i>' . do_shortcode($content) . '</a></div>';
}
}
add_shortcode('facebook', 'facebook');

if (!function_exists('google')) {
function google($atts, $content = null) {
extract(shortcode_atts(array('href' => ''), $atts));
	return '<div class="min_ikon"><a href="'.$href .'"><i class="fa fa-google-plus"></i>' . do_shortcode($content) . '</a></div>';
}
}
add_shortcode('google', 'google');

if (!function_exists('rss')) {
function rss($atts, $content = null) {
extract(shortcode_atts(array('href' => ''), $atts));
	return '<div class="min_ikon"><a href="'.$href .'"><i class="fa fa-rss"></i>' . do_shortcode($content) . '</a></div>';
}
}
add_shortcode('rss', 'rss');

if (!function_exists('twitter')) {
function twitter($atts, $content = null) {
extract(shortcode_atts(array('href' => ''), $atts));
	return '<div class="min_ikon"><a href="'.$href .'"><i class="fa fa-twitter"></i></i>' . do_shortcode($content) . '</a></div>';
}
}
add_shortcode('twitter', 'twitter');

if (!function_exists('text_normal')) {
function text_normal($atts, $content = null) {
extract(shortcode_atts(array('href' => ''), $atts));
	return '<span class="text_normal">' . do_shortcode($content) . '</span>';
}
}
add_shortcode('text_normal', 'text_normal');

if (!function_exists('white')) {
function white($atts, $content = null) {
extract(shortcode_atts(array('href' => ''), $atts));
	return '<span class="white">' . do_shortcode($content) . '</span>';
}
}
add_shortcode('white', 'white');


function list_check($atts, $content = null) {
extract(shortcode_atts(array('title' => '', 'title_min' => '', 'price' => '','price_min' => '', 'href'=>''), $atts));
    return '<div class="col-lg-3 col-sm-6 col-xs-6">
                  <div class="cont">
                    <div class="header row"> 
                        <div class="col-md-8 col-xs-6 text-center">
                            <h2>'. $title .'</h2>
                            <p>'. $title_min .'</p>
                        </div>
                        <div class="col-md-4 col-xs-6">
                            <span class="glyphicon glyphicon-arrow-left"></span>
                            <div class="text-center">
                                <h4>'. $price .'</h4>
                                <p>'. $price_min.'</p>
                            </div>
                        </div>    
                    </div>
					<div class="row text-center">'.do_shortcode($content).'</div>
						<div class="row text-center">
							<a href="'. $href .'"><button>SIGNUP NOW!</button></a>
						</div>
					</div></div>';
}
add_shortcode('list-check', 'list_check');

if (!function_exists('domena')) {
function domena($atts, $content = null) {
extract(shortcode_atts(array('domena' => ''), $atts));
	return '<div class="col-md-3 col-xs-3 domain text-center">
                            <h1>'. $domena .'</h1>
                            <h4>'. do_shortcode($content) .'</h4>
							</div>';
}
}
add_shortcode('domena', 'domena');

if (!function_exists('title')) {
function title($atts, $content = null) {
extract(shortcode_atts(array('domena' => ''), $atts));
	return '<div class="title_page text-center">
                <h1>'. do_shortcode($content) .'</h1>
			</div>';
}
}
add_shortcode('title', 'title');

if (!function_exists('row')) {
function row($atts, $content = null) {
	extract(shortcode_atts(array('domena' => ''), $atts));
	return '<div class="row">
                '. do_shortcode($content) .'
			</div>';
}
}
add_shortcode('row', 'row');

if (!function_exists('col_md_6')) {
function col_md_6($atts, $content = null) {
	extract(shortcode_atts(array('domena' => ''), $atts));
	return '<div class="col-lg-6 col-md-6 col-xs-6">
                '. do_shortcode($content) .'
			</div>';
}
}
add_shortcode('col_md_6', 'col_md_6');

if (!function_exists('our_skills')) {
function our_skills($atts, $content = null) {
	extract(shortcode_atts(array('domena' => ''), $atts));
	return '</div><div class="bg_our_skils">
                '. do_shortcode($content) .'
			</div>';
}
}
add_shortcode('our_skills', 'our_skills');

/* Dividers */
if (!function_exists('divider_10')) {
function divider_10($atts, $content = null) {
	extract(shortcode_atts(array('domena' => ''), $atts));
	return '<div class="divider-10"></div>';
}
}
add_shortcode('divider_10', 'divider_10');

if (!function_exists('divider_20')) {
function divider_20($atts, $content = null) {
	extract(shortcode_atts(array('domena' => ''), $atts));
	return '<div class="divider-20"></div>';
}
}
add_shortcode('divider_20', 'divider_20');

if (!function_exists('divider_30')) {
function divider_30($atts, $content = null) {
	extract(shortcode_atts(array('domena' => ''), $atts));
	return '<div class="divider-30"></div>';
}
}
add_shortcode('divider_30', 'divider_30');

