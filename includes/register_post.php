<?php
add_action(	'init', 'post_type_offers' );
function post_type_offers() {
	register_post_type(
		'offer',
		array(
			'labels' => array(
				'name' => 'Promocje',
				'singular_name' => 'Promocje',
				'add_new' => 'Dodaj Nową Promocję',
				'add_new_item' => 'Dodaj Nową Promocję',
				'edit_item' => 'Edytuj Promocję',
				'new_item' => 'Nowa Promocja',
				'view_item' => 'Wyświetl Promocję',
				'search_items' => 'Szukaj ',
				'not_found' => 'Nie znaleziono żadnych ',
				'not_found_in_trash' => 'Nie znaleziono żadnych w Koszu',
				'menu_name' => 'Promocja',
				),
			'capability_type' => 'post',
			'description' => 'Promocja dostępne na stronie',
			'public' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'has_archive' => true,
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'custom-fields',
				'revisions',
				'thumbnail'
				),
			'rewrite' => array(
				'slug' => 'promocja'
				)
			)
		);
}


function post_tyle_client_reviews() {
	register_post_type(
		'reviews',
		array(
			'labels' => array(
				'name' => 'Opinie',
				'singular_name' => 'Opinie',
				'add_new' => 'Dodaj Opinie',
				'add_new_item' => 'Dodaj Opinie',
				'edit_item' => 'Edytuj Opinie',
				'new_item' => 'Nowa Opinie',
				'view_item' => 'Wyświetl',
				'search_items' => 'Szukaj ',
				'not_found' => 'Nie znaleziono ',
				'not_found_in_trash' => 'Nie znaleziono w Koszu',
				'menu_name' => 'Opinie',
				),
			'capability_type' => 'post',
			'description' => 'Opinie dostępne na stronie',
			'public' => true,
			'show_ui' => true,
			'hierarchical' => false,
			'has_archive' => true,
			'supports' => array(
				'title',
				'editor',
				'excerpt',
				'custom-fields',
				'revisions',
				'thumbnail'
				),
			'rewrite' => array(
				'slug' => 'Opinie'
				)
			)
		);
}

add_action(	'init', 'post_tyle_client_reviews' );