<?php 
	function hosting_widget_init() {

	register_sidebar( array(
		'name' 			=> __('Oferta'),
		'id'            => 'sidebar-1',
		'description'   => __( 'Appears in the footer section of the site.', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	
	register_sidebar( array(
		'name' 			=> __('Lewa kolumna Top Bar'),
		'id'            => 'sidebar-8',
		'description'   => __( 'Appears in the footer section of the site.', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	
	register_sidebar( array(
		'name' 			=> __('Lewa kolumna Top Bar'),
		'id'            => 'sidebar-9',
		'description'   => __( 'Appears in the footer section of the site.', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
		register_sidebar( array(
		'name' 			=> __('Prawa kolumna Top Bar'),
		'id'            => 'sidebar-10',
		'description'   => __( 'Appears in the footer section of the site.', 'blog_edu' ),
		'before_widget' => '<aside style="float:right;" id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name' 			=> __('Oferta Domen'),
		'id'            => 'sidebar-11',
		'description'   => __( 'Oferta', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h1 class="widget-title">',
		'after_title'   => '</h1>',
	) );
	register_sidebar( array(
		'name' 			=> __('Footer-1'),
		'id'            => 'grve-footer-1-sidebar',
		'description'   => __( 'Oferta', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name' 			=> __('Footer-2'),
		'id'            => 'grve-footer-2-sidebar',
		'description'   => __( 'Oferta', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name' 			=> __('Footer-3'),
		'id'            => 'grve-footer-3-sidebar',
		'description'   => __( 'Oferta', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	register_sidebar( array(
		'name' 			=> __('Footer-4'),
		'id'            => 'grve-footer-4-sidebar',
		'description'   => __( 'Oferta', 'blog_edu' ),
		'before_widget' => '<aside id="%1$s" class="widget %2$s">',
		'after_widget'  => '</aside>',
		'before_title'  => '<h3 class="widget-title">',
		'after_title'   => '</h3>',
	) );
	
	
	
}
add_action( 'widgets_init', 'hosting_widget_init' );