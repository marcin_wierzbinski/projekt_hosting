<?php 
add_action('admin_menu', 'settings_salon');
function settings_salon() {

	add_menu_page('Hosting Ustawienia', 'Hosting Ustawienia', 'administrator', __FILE__, 'settings_salon_page');

	add_action( 'admin_init', 'register_mysettings' );
}
function register_mysettings() {
	register_setting( 'settings-group', 'section1' );
	register_setting( 'settings-group', 'section2' );
	register_setting( 'settings-group', 'section3' );
	register_setting( 'settings-group', 'section4' );
	register_setting( 'settings-group', 'section5' );
}
function settings_salon_page() {
?>
<div class="wrap">
<h2>Hosting</h2>
<form method="post" action="options.php">
    <?php settings_fields( 'settings-group' ); ?>
    <?php do_settings_sections( 'settings-group' ); ?>
    <table class="form-table">
        <tr valign="top">
			<th scope="row"><?php _e( 'Nagłówek Section1', 'textdomain' ); ?></th>
				<td><input type="text" name="section1" value="<?php echo get_option('section1'); ?>" /></td>
        </tr>
		<tr valign="top">
			<th scope="row"><?php _e( 'Nagłówek Section2', 'textdomain' ); ?></th>
				<td><input type="text" name="section2" value="<?php echo get_option('section2'); ?>" /></td>
        </tr>
		<tr valign="top">
			<th scope="row"><?php _e( 'Nagłówek Section3', 'textdomain' ); ?></th>
				<td><input type="text" name="section3" value="<?php echo get_option('section3'); ?>" /></td>
        </tr>
		<tr valign="top">
			<th scope="row"><?php _e( 'Nagłówek Section4', 'textdomain' ); ?></th>
				<td><input type="text" name="section4" value="<?php echo get_option('section4'); ?>" /></td>
        </tr>
		<tr valign="top">
			<th scope="row"><?php _e( 'Paragraf', 'textdomain' ); ?></th>
				<td><input type="text" name="section5" value="<?php echo get_option('section5'); ?>" /></td>
        </tr>
    </table>
    <?php submit_button(); ?>
</form>
</div>
<?php } 